import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Post} from '../models/Post';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class PostService {
  postsURL: string = 'https://jsonplaceholder.typicode.com/users';

  constructor(private http: HttpClient) {
  }

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.postsURL);
  }

  savePost(post: Post): Observable<Post> {
    return this.http.post<Post>(this.postsURL, post, httpOptions);
  }

  updatePost(post: Post): Observable<Post> {
    const url = `${this.postsURL}`;
    return this.http.post<Post>(url, post, httpOptions);

  }
}
